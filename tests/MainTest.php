<?php
namespace Tests;

use mrblue\quote\QuoteClient;
use mrblue\quote\Adapter\MongoDBAdapter;
use mrblue\quote\Adapter\AbstractAdapter;

class MainTest extends \PHPUnit\Framework\TestCase {
	
	public function adapterProvider()
	{
		$MongoDB = new \MongoDB\Client('mongodb://mongodb');
		$Collection_1 = $MongoDB->selectCollection('test', 'test_1');
		$Collection_1->drop();
		
		return [
			[
				new MongoDBAdapter($Collection_1,[
					'name_prefix' => 'quote_'
				])
			],
			[
				new MongoDBAdapter($Collection_1,[
					'target_document' => 333,
					'quantity_field' => 'mrblue-quote.qty.%s',
					'expire_field' => 'mrblue-quote.qty.expire_at_%s'
				])
			]
		];
	}
	
	/**
	 * @dataProvider adapterProvider
	 */
	public function testExpiration1stCount( AbstractAdapter $Adapter )
	{
		$QuoteClient = new QuoteClient($Adapter, [
			'period_mode' => QuoteClient::PERIOD_MODE_EXPIRATION,
			'period_start_mode' => QuoteClient::PERIOD_START_MODE_1ST_COUNT,
			'Timezone' => new \DateTimeZone('Europe/Rome')
		]);
		
		$k = 'test_expiration_1st_count';
		
		$QuoteClient->getLimits()->set('*', 100);
		
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 30 , $QuoteClient->incr($k, 30) );
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 60 , $QuoteClient->incr($k, 30) );
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 90 , $QuoteClient->incr($k, 30) );
		$this->assertFalse($QuoteClient->can($k, 30));
		$this->assertFalse( $QuoteClient->incr($k, 30) );
		$this->assertEquals( 50 , $QuoteClient->decr($k, 40) );
		$this->assertTrue($QuoteClient->can($k, 50));
		$this->assertEquals( 100 , $QuoteClient->incr($k, 50) );
	}
	
	/**
	 * @dataProvider adapterProvider
	 */
	public function testExpirationTimestamp( AbstractAdapter $Adapter )
	{
		$QuoteClient = new QuoteClient($Adapter, [
			'period_mode' => QuoteClient::PERIOD_MODE_EXPIRATION,
			'period_start_mode' => QuoteClient::PERIOD_START_MODE_TIMESTAMP,
			'Timezone' => new \DateTimeZone('Europe/Rome')
		]);
		
		$k = 'test_expiration_timestamp';
		
		$QuoteClient->getLimits()->set('*', 100);
		
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 30 , $QuoteClient->incr($k, 30) );
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 60 , $QuoteClient->incr($k, 30) );
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 90 , $QuoteClient->incr($k, 30) );
		$this->assertFalse($QuoteClient->can($k, 30));
		$this->assertFalse( $QuoteClient->incr($k, 30) );
		$this->assertEquals( 50 , $QuoteClient->decr($k, 40) );
		$this->assertTrue($QuoteClient->can($k, 50));
		$this->assertEquals( 100 , $QuoteClient->incr($k, 50) );
	}
	
	/**
	 * @dataProvider adapterProvider
	 */
	public function testNoPeriod( AbstractAdapter $Adapter )
	{
		$QuoteClient = new QuoteClient($Adapter, [
			'period_mode' => QuoteClient::PERIOD_MODE_NONE,
			'Timezone' => new \DateTimeZone('Europe/Rome')
		]);
		
		$k = 'test_noperiod';
		
		$QuoteClient->getLimits()->set('*', 100);
		
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 30 , $QuoteClient->incr($k, 30) );
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 60 , $QuoteClient->incr($k, 30) );
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 90 , $QuoteClient->incr($k, 30) );
		$this->assertFalse($QuoteClient->can($k, 30));
		$this->assertFalse( $QuoteClient->incr($k, 30) );
		$this->assertEquals( 50 , $QuoteClient->decr($k, 40) );
		$this->assertTrue($QuoteClient->can($k, 50));
		$this->assertEquals( 100 , $QuoteClient->incr($k, 50) );
	}
	
	/**
	 * @dataProvider adapterProvider
	 */
	public function testKeyname( AbstractAdapter $Adapter )
	{
		$QuoteClient = new QuoteClient($Adapter, [
			'period_mode' => QuoteClient::PERIOD_MODE_KEYNAME,
			'period_start_mode' => QuoteClient::PERIOD_START_MODE_TIMESTAMP,
			'Timezone' => new \DateTimeZone('Europe/Rome')
		]);
		
		$k = 'test_keyname';
		
		$QuoteClient->getLimits()->set('*', 100);
		
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 30 , $QuoteClient->incr($k, 30) );
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 60 , $QuoteClient->incr($k, 30) );
		$this->assertTrue($QuoteClient->can($k, 30));
		$this->assertEquals( 90 , $QuoteClient->incr($k, 30) );
		$this->assertFalse($QuoteClient->can($k, 30));
		$this->assertFalse( $QuoteClient->incr($k, 30) );
		$this->assertEquals( 50 , $QuoteClient->decr($k, 40) );
		$this->assertTrue($QuoteClient->can($k, 50));
		$this->assertEquals( 100 , $QuoteClient->incr($k, 50) );
	}
	
	/**
	 * @dataProvider adapterProvider
	 */
	public function testKeynameException( AbstractAdapter $Adapter )
	{
		$this->expectException( \InvalidArgumentException::class );
		
		$QuoteClient = new QuoteClient($Adapter, [
			'period_mode' => QuoteClient::PERIOD_MODE_KEYNAME,
			'period_start_mode' => QuoteClient::PERIOD_START_MODE_1ST_COUNT,
			'Timezone' => new \DateTimeZone('Europe/Rome')
		]);
	}
	
}