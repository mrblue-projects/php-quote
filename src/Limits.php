<?php
namespace mrblue\quote;

class Limits
{
	private $Parent = false;
	
	private $map = [];
	
	public function construct( array $map = [] )
	{
		$this->setMap( $map );
	}
	
	public function setMap( array $map )
	{
		foreach ( $map as $entity => $config ){
			if( is_int($config) ){
				$this->set($entity, $config, '*');
			} elseif( is_array($config) ){
				foreach ( $config as $action => $quantity ){
					$this->set($entity, $quantity, $action);
				}
			} else {
				throw new \InvalidArgumentException('Entity field content must be int or array');
			}
		}
		
		return $this;
	}
	
	function set( string $entity , int $quantity , string $action = '*' )
	{
		$this->map[ $entity ][ $action ] = $quantity;
		return $this;
	}
	
	function get( string $entity , string $action = null )
	{
		$entity_config = $this->map[$entity] ?? $this->map['*'] ?? null;
		
		if( $entity_config === null ){
			return null;
		}
		
		$limit = $entity_config[ $action ?? '*' ] ?? $entity_config[ '*' ] ?? null;
		
		return $limit;
	}
}

