<?php
namespace mrblue\quote;

use mrblue\quote\Adapter\AbstractAdapter;

class QuoteClient
{
	CONST PERIOD_MODE_EXPIRATION = 'EXPIRATION';
	CONST PERIOD_MODE_KEYNAME = 'KEYNAME';
	CONST PERIOD_MODE_NONE = 'NONE';
	
	CONST PERIOD_START_MODE_TIMESTAMP = 'TIMESTAMP';
	CONST PERIOD_START_MODE_1ST_COUNT = '1ST_COUNT';
	
	/**
	 * @var AbstractAdapter
	 */
	private $Adapter;

	private $interval = 'PT1H';

	private $period_mode = self::PERIOD_MODE_EXPIRATION;
	private $period_start_mode = self::PERIOD_START_MODE_1ST_COUNT;
	
	/**
	 * 
	 * @var Limits
	 */
	private $Limits;
	
	/**
	 * @var \DateTimeZone
	 */
	private $Timezone;
	
	private $initialized = false;
	/**
	 * @var \DateInterval
	 */
	private $Interval;
	private $time_unit;
	private $time_unit_size;
	
	private $StartPeriod;
	private $EndPeriod;
	private $name_suffix;
	private $incr_anyway;
	
	/**
	 * 
	 * @param AbstractAdapter $Adapter
	 * @param \DateInterval $Interval
	 * @param array $options values can be
	 *  string $interval
	 *  string $period_mode
	 *  string $period_start_mode
	 *  array $limits
	 *  array \DateTimeZone $Timezone
	 */
	function __construct( AbstractAdapter $Adapter, array $options )
	{
		$this->Adapter = $Adapter;
		
		if( isset($options['interval']) ){
			if( ! preg_match('/^P((\d{1,3}[YMDW])|(T\d{1,3}[HMS]))$/', $options['interval']) ){
				throw new \InvalidArgumentException('interval format must be a valid regex P((\d{1,3}[YMDW])|(T\d{1,3}[HMS]))');
			}
			$this->interval = $options['interval'];
		}
		$this->Interval = new \DateInterval($this->interval);
		
		$this->Limits = new Limits();
		
		if( isset($options['period_mode']) ){
			$this->period_mode = constant( 'self::PERIOD_MODE_'.$options['period_mode'] )or die('period_mode not valid');
		}
		if( isset($options['period_start_mode']) ){
			$this->period_start_mode = constant( 'self::PERIOD_START_MODE_'.$options['period_start_mode'] )or die('period_start_mode not valid');
		}
		if( isset($options['limits']) ){
			$this->Limits->setMap( $options['limits'] );
		}
		if( isset($options['incr_anyway']) ){
			$this->incr_anyway = (bool) $options['incr_anyway'];
		}
		
		if( isset($options['Timezone']) ){
			if( ! $options['Timezone'] instanceof \DateTimeZone ){
				throw new \InvalidArgumentException('Timezone must be instance of DateTimeZone');
			}
			$this->Timezone = $options['Timezone'];
		} else {
			$this->Timezone = new \DateTimeZone('UTC');
		}
		
		if( $this->period_mode === self::PERIOD_MODE_KEYNAME && $this->period_start_mode === self::PERIOD_START_MODE_1ST_COUNT ){
			throw new \InvalidArgumentException('PERIOD_MODE_KEYNAME and PERIOD_START_MODE_1ST_COUNT are not compatible');
		}
	}
	
	/**
	 * @return \mrblue\quote\Limits
	 */
	public function getLimits()
	{
		return $this->Limits;
	}
	
	function can( string $entity , int $quantity , $action = null )
	{
		if( $quantity <= 0 ){
			throw new \InvalidArgumentException('quantity must be int > 0 ');
		}
		
		$this->initialize();
		
		$limit = $this->Limits->get($entity, $action);
		if( $limit === null ){
			return true;
		}
		
		
		$name = $this->getName($entity , $action);
		if( $quantity > $limit ){
			return false;
		}
		
		return $this->Adapter->can( $name , $quantity , $limit );
	}
	
	function incr( string $entity , int $quantity , $action = null )
	{
		$this->initialize();
		
		$limit = $this->Limits->get($entity, $action);
		
		if( $limit && $quantity > $limit && ! $this->incr_anyway ){
			return false;
		}
		
		$name = $this->getName($entity , $action);
		
		if( $this->period_mode === self::PERIOD_MODE_EXPIRATION ){
			$ExpireTimestamp = $this->EndPeriod;
		} else {
			$ExpireTimestamp = null;
		}
		
		if( $this->incr_anyway ){
			$adapter_limit = null;
		} else {
			$adapter_limit = $limit;
		}

		$new_quantity = $this->Adapter->incr($name, $quantity, $adapter_limit, $ExpireTimestamp);

		if( $this->incr_anyway ){
			if( $new_quantity > $limit ){
				return false;
			}
		}

		return $new_quantity;
	}
	
	function decr( string $entity , int $quantity , $action = null )
	{
		$this->initialize();
		
		$name = $this->getName($entity , $action);
		
		return $this->Adapter->decr($name, $quantity);
	}
	
	private function initialize()
	{
		if( $this->initialized ){
			return;
		}
		
		$this->splitTimeUnit();
		
		if( $this->period_mode === self::PERIOD_MODE_NONE ){
			return;
		}
		
		if( $this->period_start_mode === self::PERIOD_START_MODE_1ST_COUNT ){
			  $this->StartPeriod = new \DateTime();
		} else {
			$this->StartPeriod = $this->getStartPeriod();
		}
			
		$this->EndPeriod = clone $this->StartPeriod;
		$this->EndPeriod->add( $this->Interval );
		
		if( $this->period_mode === self::PERIOD_MODE_KEYNAME ){
			$this->name_suffix = $this->getNameSuffix();
		}
		
		$this->initialized = true;
	}
	
	private function splitTimeUnit()
	{
		$interval = $this->interval;
		
		$time_unit = $interval[-1];
		$interval = substr($interval, 1, -1);
		
		if( $interval[0] === 'T' ){
			$this->time_unit_size = (int) substr($interval, 1);
			if( $time_unit === 'S' ){
				$this->time_unit = 'second';
			} elseif( $time_unit === 'M' ){
				$this->time_unit = 'minute';
			} elseif( $time_unit === 'H' ){
				$this->time_unit = 'hour';
			} else {
				throw new \LogicException("time_unit value can not be $time_unit here");
			}
		} else {
			$this->time_unit_size = (int) $interval;
			
			if( $time_unit === 'W' ){
				$this->time_unit = 'week';
			} elseif( $time_unit === 'D' ){
				$this->time_unit = 'day';
			} elseif( $time_unit === 'M' ){
				$this->time_unit = 'month';
			} elseif( $time_unit === 'H' ){
				$this->time_unit = 'year';
			} else {
				throw new \LogicException("time_unit value can not be $time_unit here");
			}
		}
		
		
	}

	private function getStartPeriod()
	{
		$Now = new \DateTime('now',$this->Timezone);
		
		$Y = (int) $Now->format('Y');
		$m = (int) $Now->format('m');
		$d = (int) $Now->format('d');
		$H = (int) $Now->format('H');
		$i = (int) $Now->format('i');
		$s = (int) $Now->format('s');
		$W = (int) $Now->format('W');
		
		if( $this->time_unit_size === 1 ){
			
			$StartPeriod = clone $Now;
			
			if( $this->time_unit === 'second' ){
				
			} elseif( $this->time_unit === 'minute' ){
				
				$StartPeriod->setTime( $H, $i , 0 );
				
			} elseif( $this->time_unit === 'hour' ){
				
				$StartPeriod->setTime( $H , 0 , 0 );
				
			} elseif( $this->time_unit === 'week' ){
				
				$StartPeriod->setTime(0, 0, 0);
				$StartPeriod->setISODate( $Y , $W, 1 );
				
			} elseif( $this->time_unit === 'day' ){
				
				$StartPeriod->setTime( 0 , 0 , 0 );
				
			} elseif( $this->time_unit === 'month' ){
				
				$StartPeriod->setTime(0, 0, 0);
				$StartPeriod->setDate( $Y , $m, 1 );
				
			} elseif( $this->time_unit === 'year' ){
				
				$StartPeriod->setTime(0, 0, 0);
				$StartPeriod->setDate( $Y , 1, 1);
				
			} else {
				throw new \LogicException("time_unit value can not be {$this->time_unit} here");
			}
			
		} else {
			
			$StartTimestamp = clone $Now;
			
			if( in_array($this->time_unit , ['second','minute','hour']) ){
				
				$StartTimestamp->setTime(0, 0, 0);
				
			} elseif( in_array($this->time_unit , ['day','month']) ){
				
				$StartTimestamp->setTime(0, 0, 0);
				$StartTimestamp->setDate( $Y , 1, 1);
				
			} elseif( $this->time_unit === 'week' ){
				
				$StartTimestamp->setTime(0, 0, 0);
				$StartTimestamp->setISODate( $Y , 1, 1);
				$StartTimestamp->modify('-1 weeks');
				
			} elseif( $this->time_unit === 'year' ){
				
				$StartTimestamp->setTime(0, 0, 0);
				$StartTimestamp->setDate( 2000 , 1, 1);
				
			} else {
				throw new \LogicException("time_unit value can not be {$this->time_unit} here");
			}
			
			while( $StartTimestamp <= $Now ){
				$StartTimestamp->add($this->Interval);
			}
			
			$StartTimestamp->sub($this->Interval);
			$StartPeriod = clone $StartTimestamp;
		}
		
		return $StartPeriod;
	}

	private function getNameSuffix()
	{
		$Y = $this->StartPeriod->format('Y');
		$o = $this->StartPeriod->format('o');
		$m = $this->StartPeriod->format('m');
		$d = $this->StartPeriod->format('d');
		$H = $this->StartPeriod->format('H');
		$i = $this->StartPeriod->format('i');
		$s = $this->StartPeriod->format('s');
		$W = $this->StartPeriod->format('W');
		
		
		if( $this->time_unit === 'second' ){
			
			return "$Y$m$d$H$i$s";
			
		} elseif( $this->time_unit === 'minute' ){
			
			return "$Y$m$d$H$i";
			
		} elseif( $this->time_unit === 'hour' ){
			
			return "$Y$m$d$H";
			
		} elseif( $this->time_unit === 'week' ){
			
			return "$o$W";
			
		} elseif( $this->time_unit === 'day' ){
			
			return "$Y$m$d";
			
		} elseif( $this->time_unit === 'month' ){
			
			return "$Y$m";
			
		} elseif( $this->time_unit === 'year' ){
			
			return $Y;
			
		} else {
			throw new \LogicException("time_unit value can not be {$this->time_unit} here");
		}
	}

	private function getName( string $entity , $action = null )
	{
		return $entity.($action ? '-'.$action : '') . ($this->name_suffix ? '-'.$this->name_suffix : '');
	}
}

