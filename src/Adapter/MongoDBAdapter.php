<?php
namespace mrblue\quote\Adapter;

class MongoDBAdapter extends AbstractAdapter
{
	private $MongoDBCollection;
	
	private $target_document = null;
	private $name_prefix = null;
	private $quantity_field = 'quantity';
	private $expire_field = 'expire_at';
	private $high_precision_expire = false;
	
	function __construct( \MongoDB\Collection $MongoDBCollection , array $options = [])
	{
		$this->MongoDBCollection = $MongoDBCollection;
		
		if( isset($options['target_document']) ){
			$this->target_document = $options['target_document'];
		}
		
		if( isset($options['name_prefix']) ){
			$this->name_prefix = $options['name_prefix'];
		}
		
		if( isset($options['quantity_field']) ){
			$this->quantity_field = $options['quantity_field'];
		}
		
		if( isset($options['expire_field']) ){
			$this->expire_field = $options['expire_field'];
		}
		
		if( isset($options['high_precision_expire']) ){
			$this->high_precision_expire = (bool) $options['high_precision_expire'];
		}
	}
	
	function can( string $name , int $quantity , int $limit )
	{
		$quantity_field = $this->getQuantityField($name);
		$expire_field = $this->getExpireField($name);
		
		$document = $this->MongoDBCollection->findOne([
			'_id' => $this->target_document ? : $this->getNameField($name),
			'$or' => [
				[
					$expire_field => null
				],
				[
					$expire_field => [
						'$gt' => new \MongoDB\BSON\UTCDateTime()
					]
				]
			]
			
		]);
		
		if ( ! $document ){
			return true;
		}
		
		$value = $this->getNestedField( (array) $document, $quantity_field);
		
		return ! $value ||
			( $value <= ($limit - $quantity) );
	}
	
	function incr( string $name , int $quantity , int $limit = null , \DateTime $ExpireTimestamp = null )
	{
		$quantity_field = $this->getQuantityField($name);
		$expire_field = $this->getExpireField($name);
		
		$_id = $this->target_document ? : $this->getNameField($name);
		
		$query = [
			'_id' => $_id
		];
		$update = [
			'$inc' => [
				$quantity_field => $quantity
			]
		];
		
		if( $ExpireTimestamp ){
			$update['$setOnInsert'] = [
				$expire_field => new \MongoDB\BSON\UTCDateTime($ExpireTimestamp)
			];
		}
		
		if( $limit ){	
			$query['$or'][][$quantity_field] = null;
			$query['$or'][][$quantity_field]['$lte'] = $limit - $quantity;
		}
		
		if( $this->high_precision_expire ){
			$this->MongoDBCollection->deleteOne([
				'_id' => $_id,
				$expire_field => [
					'$lte' => new \MongoDB\BSON\UTCDateTime()
				]
			]);
		}
		
		try{
			$document = $this->MongoDBCollection->findOneAndUpdate($query, $update, [
				'projection' => [
					$quantity_field => true
				],
				'returnDocument' => \MongoDB\Operation\FindOneAndUpdate::RETURN_DOCUMENT_AFTER,
				'upsert' => true
			]);
		} catch (\MongoDB\Driver\Exception\CommandException $e) {
			
			if( $e->getCode() == 11000 ){
				return false;
			} else {
				throw $e;
			}
		}
		
		if( ! $document ){
			return false;
		}
		
		return (int) $this->getNestedField( (array) $document, $quantity_field);
	}
	
	function decr( string $name , int $quantity )
	{
		$quantity_field = $this->getQuantityField($name);
		
		$document = $this->MongoDBCollection->findOneAndUpdate([
			'_id' => $this->target_document ? : $this->getNameField($name)
		], [
			'$inc' => [
				$quantity_field => - $quantity
			]
		],[
			'projection' => [
				$quantity_field => true
			],
			'returnDocument' => \MongoDB\Operation\FindOneAndUpdate::RETURN_DOCUMENT_AFTER,
			'upsert' => true
		]);
		
		return (int) $this->getNestedField( (array) $document, $quantity_field);
	}
	
	private function getNameField( string $name )
	{
		return $this->name_prefix . $name;
	}
	
	private function getQuantityField( string $name )
	{
		return sprintf($this->quantity_field , $name);
	}
	
	private function getExpireField( string $name )
	{
		return sprintf($this->expire_field , $name);
	}
	
	private function getNestedField( array $array , string $field )
	{
		$chain = explode('.', $field);
		
		while( $chain ){
			$index = array_shift($chain);
			$array = $array[$index] ?? null;
		}
		
		return $array;
	}
}

