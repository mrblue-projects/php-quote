<?php
namespace mrblue\quote\Adapter;

abstract class AbstractAdapter
{
	
	/**
	 * 
	 * @param string $name
	 * @param int $quantity
	 * @param int $limit
	 * @return boolean
	 */
	abstract function can( string $name , int $quantity , int $limit );
	
	/**
	 * 
	 * @param string $name
	 * @param int $quantity
	 * @param int $limit = null . Pass if limit is set
	 * @return int|bool if incr success return new value, otherwise return false
	 */
	abstract function incr( string $name , int $quantity , int $limit = null , \DateTime $ExpireTimestamp = null );
	
	abstract function decr( string $name , int $quantity );
}

